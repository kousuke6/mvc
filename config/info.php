<?php
// $info['ENV'] = 'pro';
// $info['ENV'] = 'dev';

// $info['env'] = true;//本番
$info['env'] = false;//開発

$info['debug'] = false;//デバッグ無し
// $info['debug'] = true;//デバッグ有り

$sub_domain = ($info['env']) ? end(explode('/', $_SERVER['DOCUMENT_ROOT'])) : explode('/',$_SERVER['REQUEST_URI'])[3];
$hp = ($info['env']) ? explode('/',$_SERVER['REQUEST_URI'])[1] : explode('/',$_SERVER['REQUEST_URI'])[2];

$info['PATH']['a'] = ($info['env']) ? '' : '';
$info['PATH']['b'] = ($info['env']) ? '' : '';
$info['PATH']['c'] = ($info['env']) ? '' : '';//現状なぜか相対パスじゃないとうまくアップロードできないのでこうしてる
$info['PATH']['d'] = ($info['env']) ? '' : '';
$info['PATH']['e'] = ($info['env']) ? '':'';

$info['db']['test'] = ($info['env']) ? 
array //本番環境
(
  'host' => '',
  'user' => '',
  'pass' => '',
  'dbname' => ''
)
:
array//テスト環境
(
  'host' => '',
  'user' => '',
  'pass' => '',
  'dbname' => ''
)
;


if ($info['debug'] == true) {
  ini_set('display_errors', 1);
}