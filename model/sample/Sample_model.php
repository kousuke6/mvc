<?php
class UserModel
{
    public $pdo, $shop_id;
    private $info = array();

    public function __construct($shop_id, $info)
    {
        require_once (dirname(__FILE__) . '/../config/info.php');

        try
            {
            $this->pdo = new PDO('mysql:host=' . $info['db']['']['host'] . ';dbname=' . $info['db']['']['dbname'] . ';charset=utf8', $info['db']['']['user'], $info['db']['']['pass']);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);//例外処理を投げる（スロー）ようにする
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        } catch (PDOException $e)
            {
            print ('データベースに接続できませんでした。Error:' . $e->getMessage());
            die();
        }
        
        
        $this->shop_id = $shop_id;
        $this->info = $info;
    }

    public function __destruct()
    {
        $this->pdo = null;
    }

    public function getConfig()
    {
        $stmt = $this->pdo->prepare("SELECT * FROM t_sample WHERE sampleid = :sample_id");
        $stmt->bindValue(':sample_id', $this->sample_id, PDO::PARAM_STR);
        $stmt->execute();
        $rows = $stmt->fetch(PDO::FETCH_ASSOC);
        return $rows;
    }

}