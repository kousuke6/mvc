<?php
class User
{
    private $viewHelper, $model,$config;
    private $info = array();


    public function __construct($controller_name,$actionName)
    {
        require_once(dirname(__FILE__) . '/../config/info.php');
        require_once(dirname(__FILE__) . '/../model/' . $controller_name . '_model.php');
        require_once(dirname(__FILE__) . '/../helper/view_helper.php');

        header("Content-type: text/html; charset=utf-8");
        header('X-FRAME-OPTIONS: SAMEORIGIN');
        session_start();

        $model_name = $controller_name . 'Model';
        $this->info = $info;
        $this->model = new $model_name();
        $this->viewHelper = new ViewHelper();
        $this->config = $this->model->getConfig();
        $this->ua = $_SERVER['HTTP_USER_AGENT'];

        $this->viewHelper->assign('env', $info['env']);
        $this->viewHelper->assign('config', $this->config);
        $this->viewHelper->assign('PATH', $this->info['PATH']);
        $this->viewHelper->assign('ua', $this->ua);

    }
   
    public function sample1()
    {
        
        $this->viewHelper->display("sample/sample1.html");
    }



}