<?php
// require_once (dirname(__FILE__).'/../helper/functions.php');
if(isset($_GET['url']) && explode('/', $_GET['url'])[0] != '')
{
    $params = explode('/', $_GET['url']);
    $controller_name = ucfirst(array_shift($params));
    $action_name = array_shift($params);
    require_once (dirname(__FILE__) . '/../../miseapp/controller/' . $controller_name . '.php');
    $controller = new $controller_name($controller_name,$action_name);
    $controller->$action_name();
}
else{
    require_once (dirname(__FILE__) . '/../../miseapp/controller/User.php');
    $controller_name = 'User';
    $action_name = 'usertop';
    $controller = new $controller_name($controller_name,$action_name);
    $controller->$action_name();
}
