<?php

class ViewHelper {
    private $data = array();

    public function assign($key, $value)
    {
        $this->data[$key] = $value;
    }
    public function display($view_page)
    {
        extract($this->data);
        require_once(dirname(__FILE__).'/../view/'.$view_page);
    }
}
?>